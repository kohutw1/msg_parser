////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////
`define cdiv(x,y)            ( x % y)      ? \
                             ((x / y) + 1) : \
                             ( x / y);

////////////////////////////////////////////////////////////////////
// Values
////////////////////////////////////////////////////////////////////
`define WIDTH_MSG_CNT_BYTES  2
`define WIDTH_MSG_LEN_BYTES  2

`define BITS_PER_BYTE        8

`define WIDTH_MSG_DATA_BYTES `MAX_MSG_BYTES

`define WIDTH_TDATA_BITS     (`WIDTH_TDATA_BYTES    * `BITS_PER_BYTE)
`define WIDTH_MSG_DATA_BITS  (`WIDTH_MSG_DATA_BYTES * `BITS_PER_BYTE)

`define WIDTH_MSG_LEN_BITS   $clog2(`WIDTH_MSG_DATA_BYTES + 1)
