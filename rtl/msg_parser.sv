////////////////////////////////////////////////////////////////////
// Message Parser
////////////////////////////////////////////////////////////////////
//
// Author:
//     Will Kohut (kohutw@gmail.com)

`include "global_defines.svh"

module msg_parser #(
    parameter MIN_MSG_BYTES     =  8,
    parameter MAX_MSG_BYTES     = 32,
    parameter WIDTH_TDATA_BYTES =  8
) (
    input  logic                              clk,
    input  logic                              rst,

    ////////////////////////////////////////////////////////////////////
    // AXI-Stream Slave Interface
    ////////////////////////////////////////////////////////////////////

    output logic                              s_tready,
    input  logic                              s_tvalid,
    input  logic                              s_tlast,
    input  logic [`WIDTH_TDATA_BITS    - 1:0] s_tdata,
    input  logic [`WIDTH_TDATA_BYTES   - 1:0] s_tkeep, // Unused: derived
    input  logic                              s_tuser,

    ////////////////////////////////////////////////////////////////////
    // Outputs
    ////////////////////////////////////////////////////////////////////

    output logic                              msg_valid,
    output logic [`WIDTH_MSG_LEN_BITS  - 1:0] msg_length,
    output logic [`WIDTH_MSG_DATA_BITS - 1:0] msg_data,
    output logic                              msg_error
);

    ////////////////////////////////////////////////////////////////////
    // Local parameters
    ////////////////////////////////////////////////////////////////////

    localparam BITS_PER_BYTE                   = `BITS_PER_BYTE;

    localparam WIDTH_TDATA_BITS                = `WIDTH_TDATA_BITS;

    localparam WIDTH_MSG_CNT_BYTES             = `WIDTH_MSG_CNT_BYTES;
    localparam WIDTH_MSG_LEN_BYTES             = `WIDTH_MSG_LEN_BYTES;

    localparam WIDTH_MSG_CNT_BITS              = WIDTH_MSG_CNT_BYTES * BITS_PER_BYTE;
    localparam WIDTH_MSG_LEN_BITS              = WIDTH_MSG_LEN_BYTES * BITS_PER_BYTE;

    localparam WIDTH_MSG_LEN_BYTES_HALVED      = WIDTH_MSG_LEN_BYTES / 2;
    localparam WIDTH_MSG_LEN_BITS_HALVED       = WIDTH_MSG_LEN_BYTES_HALVED * BITS_PER_BYTE;

    localparam MAX_MSG_CYC                     = `cdiv(MAX_MSG_BYTES, WIDTH_TDATA_BYTES)

    localparam WIDTH_MSG_LEN_REM_BITS          = $clog2(MAX_MSG_BYTES + 1);

    // We subtract 1 because the last word in the message never needs to be stored
    localparam NUM_MSG_BUFFER_WORDS            = MAX_MSG_CYC - 1;

    localparam WIDTH_PTR_BITS                  = $clog2(MAX_MSG_CYC);

    localparam MAX_DOWNSHIFT_BYTES             = WIDTH_TDATA_BYTES - 1;
    localparam MAX_UPSHIFT_NON_TAIL_OUT_BYTES  = WIDTH_TDATA_BYTES;
    localparam MAX_UPSHIFT_TAIL_OUT_BYTES      = WIDTH_TDATA_BYTES * 2;

    localparam WIDTH_DOWNSHIFT_BITS            = $clog2(MAX_DOWNSHIFT_BYTES + 1);
    localparam WIDTH_UPSHIFT_NON_TAIL_OUT_BITS = $clog2(MAX_UPSHIFT_NON_TAIL_OUT_BYTES + 1);
    localparam WIDTH_UPSHIFT_TAIL_OUT_BITS     = $clog2(MAX_UPSHIFT_TAIL_OUT_BYTES + 1);

    ////////////////////////////////////////////////////////////////////
    // Declarations
    ////////////////////////////////////////////////////////////////////

    logic xfer;

    logic sop_pending;

    logic get_new_msg_len_from_sop,
          get_new_msg_len_from_straddle_next,
          get_new_msg_len_from_straddle,
          get_new_msg_len_from_shift;

    logic [WIDTH_MSG_LEN_BITS - 1:0] msg_len_from_sop,
                                     msg_len_from_straddle,
                                     msg_len_from_shift;

    logic [WIDTH_MSG_LEN_REM_BITS - 1:0] msg_len_rem_bytes,
                                         msg_len_rem_bytes_next;

    logic [(WIDTH_TDATA_BITS * NUM_MSG_BUFFER_WORDS) - 1:0] data_buffer;

    logic [NUM_MSG_BUFFER_WORDS - 1:0] mask_buffer;

    logic [WIDTH_MSG_LEN_BITS_HALVED - 1:0] straddle_hi,
                                            straddle_lo;

    logic [WIDTH_PTR_BITS - 1:0] head_ptr,
                                 tail_ptr;

    logic nothing_to_store,
          nothing_to_store_next;

    logic [WIDTH_DOWNSHIFT_BITS - 1:0] head_in_downshift_bytes_next,
                                       head_in_downshift_bytes,
                                       head_in_downshift_bytes_prev;

    logic [WIDTH_UPSHIFT_NON_TAIL_OUT_BITS - 1:0] head_out_upshift_bytes,
                                                  tail_in_upshift_bytes,
                                                  tail_in_upshift_bytes_next;

    logic [WIDTH_UPSHIFT_TAIL_OUT_BITS - 1:0] tail_out_upshift_bytes;

    logic [WIDTH_TDATA_BITS - 1:0] head_out_bitmask,
                                   tail_out_bitmask;

    logic [MAX_MSG_CYC - 1:0] is_head,
                              is_head_nowrap,
                              is_tail;

    logic [WIDTH_TDATA_BYTES - 1:0] head_out_bytemask,
                                    tail_out_bytemask;

    logic [`WIDTH_MSG_DATA_BYTES - 1:0] out_bytemask;

    logic [WIDTH_TDATA_BITS - 1:0] msg_len_from_shift_wide;

    logic [WIDTH_TDATA_BITS - 1:0] head_in_data,
                                   head_in_data_out,
                                   tail_in_data;

    logic [WIDTH_TDATA_BITS - 1:0] head_out_data,
                                   tail_out_data;

    logic last_cyc_in_msg,
          last_cyc_in_msg_next,
          last_cyc_in_msg_vld;

    logic use_head;

    ////////////////////////////////////////////////////////////////////
    // Determine how to decrement remaining message length
    ////////////////////////////////////////////////////////////////////

    assign xfer = s_tvalid && s_tready;

    always_ff @(posedge clk) begin
        if(rst) begin
            sop_pending <= 1'b1;
        end else begin
            sop_pending <= (sop_pending && !xfer) || (s_tlast && xfer);
        end
    end

    assign get_new_msg_len_from_sop = sop_pending && xfer;

    assign get_new_msg_len_from_shift = msg_len_rem_bytes <= (WIDTH_TDATA_BYTES - WIDTH_MSG_LEN_BYTES);

    assign get_new_msg_len_from_straddle_next = (WIDTH_TDATA_BYTES - msg_len_rem_bytes) == WIDTH_MSG_LEN_BYTES_HALVED;

    always_ff @(posedge clk) begin
        if(rst) begin
            get_new_msg_len_from_straddle <= 1'b0;
        end else begin
            get_new_msg_len_from_straddle <= get_new_msg_len_from_straddle_next;
        end
    end

    ////////////////////////////////////////////////////////////////////
    // Prepare inputs to decrement logic
    ////////////////////////////////////////////////////////////////////

    always_ff @(posedge clk) begin
        if(rst) begin
            straddle_lo <= {WIDTH_MSG_LEN_BITS_HALVED{1'b0}};
        end else begin
            if(s_tvalid) begin
                straddle_lo <= s_tdata[WIDTH_TDATA_BITS - 1 -:WIDTH_MSG_LEN_BITS_HALVED];
            end else begin
                straddle_lo <= straddle_lo;
            end
        end
    end

    assign straddle_hi = s_tdata[WIDTH_MSG_LEN_BITS_HALVED - 1:0];

    assign msg_len_from_shift_wide = s_tdata >> (msg_len_rem_bytes * BITS_PER_BYTE);

    ////////////////////////////////////////////////////////////////////
    // Reset and decrement remaining message length
    ////////////////////////////////////////////////////////////////////

    always_comb begin
        msg_len_from_sop      = s_tdata[WIDTH_MSG_CNT_BITS + WIDTH_MSG_LEN_BITS - 1 -:WIDTH_MSG_LEN_BITS];
        msg_len_from_straddle = {straddle_hi, straddle_lo};
        msg_len_from_shift    = msg_len_from_shift_wide[WIDTH_MSG_LEN_BITS - 1:0];

        // Unique keyword shortens path by allowing parallel case evaluation
        unique casez({
            get_new_msg_len_from_sop,
            get_new_msg_len_from_straddle_next || s_tlast,
            get_new_msg_len_from_straddle,
            get_new_msg_len_from_shift
        })
            4'b1???: msg_len_rem_bytes_next = msg_len_from_sop      - (WIDTH_TDATA_BYTES - WIDTH_MSG_CNT_BYTES - WIDTH_MSG_LEN_BYTES);
            4'b01??: msg_len_rem_bytes_next = {WIDTH_MSG_LEN_REM_BITS{1'b0}};
            4'b001?: msg_len_rem_bytes_next = msg_len_from_straddle - (WIDTH_TDATA_BYTES - WIDTH_MSG_LEN_BYTES_HALVED);
            4'b0001: msg_len_rem_bytes_next = msg_len_from_shift    - (WIDTH_TDATA_BYTES - WIDTH_MSG_LEN_BYTES - msg_len_rem_bytes);
            default: msg_len_rem_bytes_next = msg_len_rem_bytes     -  WIDTH_TDATA_BYTES;
        endcase
    end

    always_ff @(posedge clk) begin
        if(rst) begin
            msg_len_rem_bytes <= {WIDTH_MSG_LEN_REM_BITS{1'b0}};
        end else begin
            if(s_tvalid) begin
                msg_len_rem_bytes <= msg_len_rem_bytes_next;
            end else begin
                msg_len_rem_bytes <= msg_len_rem_bytes;
            end
        end
    end

    ////////////////////////////////////////////////////////////////////
    // Manage buffer word pointers
    ////////////////////////////////////////////////////////////////////

    assign last_cyc_in_msg_next = (msg_len_rem_bytes_next <= WIDTH_TDATA_BYTES) && |msg_len_rem_bytes_next;

    assign nothing_to_store_next = (msg_len_rem_bytes_next >= (WIDTH_TDATA_BYTES - WIDTH_MSG_LEN_BYTES)) &&
                                   (msg_len_rem_bytes_next <=  WIDTH_TDATA_BYTES);

    always_ff @(posedge clk) begin
        if(rst) begin
            nothing_to_store <= 1'b0;
        end else begin
            if(s_tvalid) begin
                nothing_to_store <= nothing_to_store_next;
            end else begin
                nothing_to_store <= nothing_to_store;
            end
        end
    end

    always_ff @(posedge clk) begin
        if(rst) begin
            head_ptr <= {WIDTH_PTR_BITS{1'b0}};
            tail_ptr <= {WIDTH_PTR_BITS{1'b0}};
        end else begin
            if(s_tvalid) begin
                head_ptr <= (last_cyc_in_msg_next || nothing_to_store || s_tlast) ?
                            {WIDTH_PTR_BITS{1'b0}} :
                            head_ptr + 1'b1;

                tail_ptr <= head_ptr;
            end else begin
                head_ptr <= head_ptr;
                tail_ptr <= tail_ptr;
            end
        end
    end

    ////////////////////////////////////////////////////////////////////
    // Construct and apply buffer bytemasks
    ////////////////////////////////////////////////////////////////////

    assign head_in_downshift_bytes_next = msg_len_rem_bytes_next + WIDTH_MSG_LEN_BYTES;

    assign tail_in_upshift_bytes_next = WIDTH_TDATA_BYTES - head_in_downshift_bytes;

    always_ff @(posedge clk) begin
        if(rst || s_tlast) begin
            head_in_downshift_bytes_prev <= {WIDTH_DOWNSHIFT_BITS{1'b0}};
            head_in_downshift_bytes      <=                      WIDTH_MSG_CNT_BYTES + WIDTH_MSG_LEN_BYTES;
            tail_in_upshift_bytes        <= WIDTH_TDATA_BYTES - (WIDTH_MSG_CNT_BYTES + WIDTH_MSG_LEN_BYTES);
        end else begin
            if(s_tvalid && last_cyc_in_msg_next) begin
                head_in_downshift_bytes      <= head_in_downshift_bytes_next;
                head_in_downshift_bytes_prev <= head_in_downshift_bytes;
            end else begin
                head_in_downshift_bytes      <= head_in_downshift_bytes;
                head_in_downshift_bytes_prev <= head_in_downshift_bytes_prev;
            end

            if(s_tvalid && last_cyc_in_msg) begin
                tail_in_upshift_bytes <= tail_in_upshift_bytes_next;
            end else begin
                tail_in_upshift_bytes <= tail_in_upshift_bytes;
            end
        end
    end

    assign use_head = msg_len_rem_bytes > head_in_downshift_bytes_prev;

    assign head_in_data     = s_tdata >> (head_in_downshift_bytes      * BITS_PER_BYTE);
    assign head_in_data_out = s_tdata >> (head_in_downshift_bytes_prev * BITS_PER_BYTE);
    assign tail_in_data     = s_tdata << (tail_in_upshift_bytes        * BITS_PER_BYTE);

    assign head_out_upshift_bytes = msg_len_rem_bytes - head_in_downshift_bytes_prev;
    assign tail_out_upshift_bytes = msg_len_rem_bytes + tail_in_upshift_bytes;

    assign head_out_bytemask = ~({WIDTH_TDATA_BYTES{1'b1}} << head_out_upshift_bytes);
    assign tail_out_bytemask = ~({WIDTH_TDATA_BYTES{1'b1}} << tail_out_upshift_bytes);

    genvar byte_i;

    generate
        for(byte_i = 0; byte_i < WIDTH_TDATA_BYTES; byte_i = byte_i + 1) begin: out_bitmasks
            assign head_out_bitmask[BITS_PER_BYTE * byte_i +:BITS_PER_BYTE] = {BITS_PER_BYTE{head_out_bytemask[byte_i]}};
            assign tail_out_bitmask[BITS_PER_BYTE * byte_i +:BITS_PER_BYTE] = {BITS_PER_BYTE{tail_out_bytemask[byte_i]}};
        end
    endgenerate

    assign head_out_data = head_in_data_out & head_out_bitmask;
    assign tail_out_data = tail_in_data     & tail_out_bitmask;

    ////////////////////////////////////////////////////////////////////
    // Loop over all words in output
    ////////////////////////////////////////////////////////////////////
    genvar word_i;

    generate
        for(word_i = 0; word_i < MAX_MSG_CYC; word_i = word_i + 1) begin: buffer_and_output
            localparam CURR_WORD_LSBYTE = WIDTH_TDATA_BYTES * word_i;
            localparam CURR_WORD_LSBIT  = CURR_WORD_LSBYTE * BITS_PER_BYTE;

            assign is_head       [word_i] = head_ptr == word_i;
            assign is_tail       [word_i] = tail_ptr == word_i;

            assign is_head_nowrap[word_i] = (tail_ptr + 1'b1) == word_i;

            ////////////////////////////////////////////////////////////////////
            // Buffer input data stream and manage bytemask
            ////////////////////////////////////////////////////////////////////

            // Don't reset the first word after last cycle in message; overwrite
            // it instead. This prevents us from squashing new data when we wrap.
            if(word_i == 0) begin
                always_ff @(posedge clk) begin
                    if(rst) begin
                        data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= {WIDTH_TDATA_BITS{1'b0}};

                        // We assume a minimum message length of
                        // WIDTH_TDATA_BYTES and enforce with an assertion
                        mask_buffer[word_i] <= 1'b1;
                    end else begin
                        // Unique keyword shortens path by allowing parallel case evaluation
                        unique casez({
                            is_head[word_i],
                            is_tail[word_i]
                        })
                            2'b1?:   data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= head_in_data;
                            2'b01:   data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] | tail_in_data;
                            default: data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS];
                        endcase

                        mask_buffer[word_i] <= mask_buffer[word_i];
                    end
                end
            end else if(word_i < NUM_MSG_BUFFER_WORDS) begin
                always_ff @(posedge clk) begin
                    if(rst || last_cyc_in_msg_vld) begin
                        data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= {WIDTH_TDATA_BITS{1'b0}};
                        mask_buffer[word_i]                             <= 1'b0;
                    end else begin
                        // Unique keyword shortens path by allowing parallel case evaluation
                        unique casez({
                            is_head[word_i],
                            is_tail[word_i]
                        })
                            2'b1?:   data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= head_in_data;
                            2'b01:   data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] | tail_in_data;
                            default: data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] <= data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS];
                        endcase

                        mask_buffer[word_i] <= mask_buffer[word_i] | is_tail[word_i];
                    end
                end
            end

            ////////////////////////////////////////////////////////////////////
            // Drive outputs
            ////////////////////////////////////////////////////////////////////

            if(word_i < NUM_MSG_BUFFER_WORDS) begin
                always_comb begin
                    // Unique keyword shortens path by allowing parallel case evaluation
                    unique casez({
                         is_head_nowrap[word_i] && use_head,
                         is_tail       [word_i]
                    })
                        2'b1?: begin
                            msg_data    [CURR_WORD_LSBIT  +:WIDTH_TDATA_BITS ] = head_out_data;
                            out_bytemask[CURR_WORD_LSBYTE +:WIDTH_TDATA_BYTES] = head_out_bytemask;
                        end

                        2'b01: begin
                            msg_data    [CURR_WORD_LSBIT  +:WIDTH_TDATA_BITS ] = data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS] | tail_out_data;
                            out_bytemask[CURR_WORD_LSBYTE +:WIDTH_TDATA_BYTES] = tail_out_bytemask;
                        end

                        default: begin
                            msg_data    [CURR_WORD_LSBIT  +:WIDTH_TDATA_BITS ] = data_buffer[CURR_WORD_LSBIT +:WIDTH_TDATA_BITS];
                            out_bytemask[CURR_WORD_LSBYTE +:WIDTH_TDATA_BYTES] = {WIDTH_TDATA_BYTES{mask_buffer[word_i]}};
                        end
                    endcase
                end
            end else begin
                // The most significant word doesn't need mask buffer storage
                assign msg_data    [CURR_WORD_LSBIT  +:WIDTH_TDATA_BITS ] = (is_head_nowrap[word_i] && use_head) ? head_out_data     : {WIDTH_TDATA_BITS {1'b0}};
                assign out_bytemask[CURR_WORD_LSBYTE +:WIDTH_TDATA_BYTES] = (is_head_nowrap[word_i] && use_head) ? head_out_bytemask : {WIDTH_TDATA_BYTES{1'b0}};
            end
        end
    endgenerate

    always_ff @(posedge clk) begin
        if(rst) begin
            last_cyc_in_msg <= 1'b0;
        end else begin
            if(s_tvalid) begin
                last_cyc_in_msg <= last_cyc_in_msg_next;
            end else begin
                last_cyc_in_msg <= last_cyc_in_msg;
            end
        end
    end

    assign last_cyc_in_msg_vld = last_cyc_in_msg && s_tvalid;

    assign msg_valid = last_cyc_in_msg_vld;

    assign msg_length = $countones(out_bytemask);

    assign msg_error = s_tuser;

    // Serial-in-parallel-out paradigm implies that we're always ready by design
    assign s_tready = 1'b1;

    ////////////////////////////////////////////////////////////////////
    // Assertions
    ////////////////////////////////////////////////////////////////////

    `ifndef SYNTHESIS
        initial begin
            assert(MIN_MSG_BYTES <= MAX_MSG_BYTES) else
                $fatal(0, "Minimum message length (%0d bytes) must less than or equal to maximum message length (%0d bytes)", MIN_MSG_BYTES, MAX_MSG_BYTES);

            // Enforce serial-in-parallel-out paradigm
            assert(MIN_MSG_BYTES >= WIDTH_TDATA_BYTES) else
                $fatal(0, "Minimum message length (%0d bytes) must be greater than or equal to s_tdata bus width (%0d bytes)", MIN_MSG_BYTES, WIDTH_TDATA_BYTES);
        end

        // Sample assertions on negedge to avoid race conditions
        always_ff @(negedge clk) begin
            // TODO: Assert if packet is greater than 1500 bytes
            //assert() else
            //    $fatal(0, "", );

            /* TODO: These need a little fine-tuning
            assert(!(s_tvalid && get_new_msg_len_from_sop)      || (MIN_MSG_BYTES <= msg_len_from_sop)) else
                $fatal(0, "Minimum message length (%0d bytes) must be less than or equal to message length from start of packet (%0d)", MIN_MSG_BYTES, msg_len_from_sop);

            assert(!(s_tvalid && get_new_msg_len_from_sop)      || (MAX_MSG_BYTES >= msg_len_from_sop)) else
                $fatal(0, "Maximum message length (%0d bytes) must be greater than or equal to message length from start of packet (%0d)", MAX_MSG_BYTES, msg_len_from_sop);

            assert(!(s_tvalid && get_new_msg_len_from_straddle) || (MIN_MSG_BYTES <= msg_len_from_straddle)) else
                $fatal(0, "Minimum message length (%0d bytes) must be less than or equal to message length from straddle (%0d)", MIN_MSG_BYTES, msg_len_from_straddle);

            assert(!(s_tvalid && get_new_msg_len_from_straddle) || (MAX_MSG_BYTES >= msg_len_from_straddle)) else
                $fatal(0, "Maximum message length (%0d bytes) must be greater than or equal to message length from straddle (%0d)", MAX_MSG_BYTES, msg_len_from_straddle);

            assert(!(s_tvalid && get_new_msg_len_from_shift)    || (MIN_MSG_BYTES <= msg_len_from_shift)) else
                $fatal(0, "Minimum message length (%0d bytes) must be less than or equal to message length from shift (%0d)", MIN_MSG_BYTES, msg_len_from_shift);

            assert(!(s_tvalid && get_new_msg_len_from_shift)    || (MAX_MSG_BYTES >= msg_len_from_shift)) else
                $fatal(0, "Maximum message length (%0d bytes) must be greater than or equal to message length from shift (%0d)", MAX_MSG_BYTES, msg_len_from_shift);
            */
        end
    `endif

endmodule
