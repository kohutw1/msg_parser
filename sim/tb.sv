////////////////////////////////////////////////////////////////////
// Message Parser Testbench
////////////////////////////////////////////////////////////////////
//
// Author:
//     Will Kohut (kohutw@gmail.com)

`timescale 1ns / 1ps

`include "global_defines.svh"

module tb;
    ////////////////////////////////////////////////////////////////////
    // Declare DUT signals
    ////////////////////////////////////////////////////////////////////
    logic                              clk;
    logic                              rst;

    logic                              s_tready;
    logic                              s_tvalid;
    logic                              s_tlast;
    logic [`WIDTH_TDATA_BITS    - 1:0] s_tdata,
                                       s_tdata_next;
    logic [`WIDTH_TDATA_BYTES   - 1:0] s_tkeep;
    logic                              s_tuser;

    logic                              msg_valid;
    logic [`WIDTH_MSG_LEN_BITS  - 1:0] msg_length;
    logic [`WIDTH_MSG_DATA_BITS - 1:0] msg_data;
    logic                              msg_error;

    ////////////////////////////////////////////////////////////////////
    // Declare and initialize TB signals
    ////////////////////////////////////////////////////////////////////
    logic seq_started           = 1'b0;
    logic seq_ended             = 1'b0;
    logic in_ready_d1           = 1'b0;
    logic inject                = 1'b0;

    int RUN_SAMPLE_PKTS         = 0;
    int NUM_PKT                 = 1;
    int NUM_MSG_PER_PKT         = 1;

    int TVALID_PROB             = 100;

    string rand_in_stim_fn      = "rand_input_stimulus.txt";
    int    rand_in_stim_fd      = 0;

    ////////////////////////////////////////////////////////////////////
    // Read in plusargs
    ////////////////////////////////////////////////////////////////////
    initial begin
        if($value$plusargs("RUN_SAMPLE_PKTS=%d", RUN_SAMPLE_PKTS)) begin
        end else if($value$plusargs("NUM_PKT=%d", NUM_PKT) && $value$plusargs("NUM_MSG_PER_PKT=%d", NUM_MSG_PER_PKT)) begin
        end else begin
            $fatal(0, "Must provide either +RUN_SAMPLE_PKTS=<1,0> or +NUM_PKT=<num_pkt> and +NUM_MSG_PER_PKT=<num_msg_per_pkt> plusargs");
        end

        if($value$plusargs("TVALID_PROB=%d", TVALID_PROB));
    end

    ////////////////////////////////////////////////////////////////////
    // Manage clock
    ////////////////////////////////////////////////////////////////////
    // 500 MHz clock
    int CLK_PERIOD      =              2;
    int CLK_PERIOD_BY_2 = CLK_PERIOD / 2;

    initial forever #CLK_PERIOD_BY_2 clk = !clk;

    default clocking cb @(posedge clk);
        default input #1step output #0;

        output rst, s_tdata, s_tlast, s_tkeep, s_tuser;
        input  s_tready, msg_valid, msg_data, msg_length;
    endclocking

    ////////////////////////////////////////////////////////////////////
    // Test entry point
    ////////////////////////////////////////////////////////////////////
    initial begin
        clk = 1'b1;

        @cb rst <= 1'b1; // Enter reset
        @cb rst <= 1'b0; // Exit reset

        repeat(2) @cb;

        if(RUN_SAMPLE_PKTS) begin
            $display("");
            $display("////////////////////////////////////////////////////////////////////");
            $display("// RUNNING SAMPLE PACKETS");
            $display("////////////////////////////////////////////////////////////////////");
            $display("");

            @(cb iff in_ready_d1) s_tlast <= 1'b0; s_tdata <= 'habcddcef_00080001; s_tkeep <= 'b11111111; s_tuser <= 1'b0; seq_started <= 1'b1;
            @(cb iff inject     ) s_tlast <= 1'b1; s_tdata <= 'h00000000_630d658d; s_tkeep <= 'b00001111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h045de506_000e0002; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h03889560_84130858; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h85468052_0008a5b0; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b1; s_tdata <= 'h00000000_d845a30c; s_tkeep <= 'b00001111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h62626262_00080008; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h6868000c_62626262; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h68686868_68686868; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h70707070_000a6868; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h000f7070_70707070; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h7a7a7a7a_7a7a7a7a; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h0e7a7a7a_7a7a7a7a; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h4d4d4d4d_4d4d4d00; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h114d4d4d_4d4d4d4d; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h38383838_38383800; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h38383838_38383838; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h31313131_000b3838; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h09313131_31313131; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b0; s_tdata <= 'h5a5a5a5a_5a5a5a00; s_tkeep <= 'b11111111; s_tuser <= 1'b0;
            @(cb iff inject     ) s_tlast <= 1'b1; s_tdata <= 'h00000000_00005a5a; s_tkeep <= 'b00000011; s_tuser <= 1'b0;
        end else begin
            automatic int byte_cnt = 0;

            $display("");
            $display("////////////////////////////////////////////////////////////////////");
            $display("// RUNNING RANDOM PACKETS");
            $display("////////////////////////////////////////////////////////////////////");
            $display("");
            $display("!!! Dumping random input stimulus to %s !!!", rand_in_stim_fn);
            $display("");

            rand_in_stim_fd = $fopen(rand_in_stim_fn, "w");

            repeat(NUM_PKT) begin
                automatic int msg_rem = NUM_MSG_PER_PKT;

                automatic int msg_start_byte = `WIDTH_MSG_CNT_BYTES;

                s_tdata_next >>= `WIDTH_MSG_CNT_BYTES * `BITS_PER_BYTE;

                // Initialize s_tdata_next with message count
                s_tdata_next[`WIDTH_TDATA_BITS - 1 -:(`WIDTH_MSG_CNT_BYTES * `BITS_PER_BYTE)] =
                             msg_rem[(`WIDTH_MSG_CNT_BYTES * `BITS_PER_BYTE) - 1:0];

                byte_cnt += `WIDTH_MSG_CNT_BYTES;

                while(msg_rem > 0) begin
                    automatic int msg_bytes = $urandom_range(`MIN_MSG_BYTES, `MAX_MSG_BYTES);

                    automatic int first_msg = msg_rem == NUM_MSG_PER_PKT;
                    automatic int last_msg  = msg_rem == 1;

                    automatic int msg_plus_len_bytes = msg_bytes + `WIDTH_MSG_LEN_BYTES;

                    automatic int rem_bytes = (msg_plus_len_bytes + byte_cnt) % `WIDTH_TDATA_BYTES;

                    automatic int pad_bytes = `WIDTH_TDATA_BYTES - rem_bytes;

                    automatic int msg_bytes_qual = last_msg ? (msg_plus_len_bytes + pad_bytes) : msg_plus_len_bytes;

                    automatic int msg_delimiter;

                    for(int byte_i = 0; byte_i < msg_bytes_qual; byte_i++) begin
                        s_tdata_next >>= `BITS_PER_BYTE;

                        if(byte_i >= msg_plus_len_bytes) begin
                            s_tdata_next[`WIDTH_TDATA_BITS - 1 -:`BITS_PER_BYTE] = `BITS_PER_BYTE'h0;
                        end else if(byte_i == 0) begin
                            s_tdata_next[`WIDTH_TDATA_BITS - 1 -:`BITS_PER_BYTE] = msg_bytes[             0 +:`BITS_PER_BYTE];
                        end else if(byte_i == 1) begin
                            s_tdata_next[`WIDTH_TDATA_BITS - 1 -:`BITS_PER_BYTE] = msg_bytes[`BITS_PER_BYTE +:`BITS_PER_BYTE];
                        end else if(byte_i == 2) begin
                            msg_delimiter = $urandom;
                            s_tdata_next[`WIDTH_TDATA_BITS - 1 -:`BITS_PER_BYTE] = msg_delimiter;
                        end else if(byte_i == (msg_plus_len_bytes - 1)) begin
                            s_tdata_next[`WIDTH_TDATA_BITS - 1 -:`BITS_PER_BYTE] = msg_delimiter + 2;
                        end else begin
                            s_tdata_next[`WIDTH_TDATA_BITS - 1 -:`BITS_PER_BYTE] = msg_delimiter + 1;
                        end

                        byte_cnt++;

                        if(!(byte_cnt % `WIDTH_TDATA_BYTES)) begin
                            if(first_msg && (byte_i == (`WIDTH_TDATA_BYTES - `WIDTH_MSG_CNT_BYTES - 1))) begin
                                @(cb iff in_ready_d1) begin
                                    s_tdata <= s_tdata_next; s_tkeep <= {`WIDTH_TDATA_BYTES{1'b1}}; s_tlast <= 1'b0; s_tuser <= 1'b0; seq_started <= 1'b1;
                                    $fdisplay(rand_in_stim_fd, "s_tdata=%h s_tkeep=%b s_tlast=%b s_tuser=%b s_tvalid=%b", s_tdata_next, {`WIDTH_TDATA_BYTES{1'b1}}, 1'b0, 1'b0, 1'b1);
                                end
                            end else if(last_msg && (byte_i == (msg_bytes_qual - 1))) begin
                                @(cb iff inject) begin
                                    s_tdata <= s_tdata_next; s_tkeep <= {`WIDTH_TDATA_BYTES{1'b1}} >> pad_bytes; s_tlast <= 1'b1; s_tuser <= 1'b0; seq_started <= 1'b1;
                                    $fdisplay(rand_in_stim_fd, "s_tdata=%h s_tkeep=%b s_tlast=%b s_tuser=%b s_tvalid=%b", s_tdata_next, {`WIDTH_TDATA_BYTES{1'b1}} >> pad_bytes, 1'b1, 1'b0, 1'b1);
                                end
                            end else begin
                                @(cb iff inject) begin
                                    s_tdata <= s_tdata_next; s_tkeep <= {`WIDTH_TDATA_BYTES{1'b1}}; s_tlast <= 1'b0; s_tuser <= 1'b0;
                                    $fdisplay(rand_in_stim_fd, "s_tdata=%h s_tkeep=%b s_tlast=%b s_tuser=%b s_tvalid=%b", s_tdata_next, {`WIDTH_TDATA_BYTES{1'b1}}, 1'b0, 1'b0, 1'b1);
                                end
                            end
                        end
                    end

                    msg_rem--;
                end
            end

            $fclose(rand_in_stim_fd);
        end

        @(cb) seq_ended <= 1'b1;

        // Idle pipe
        repeat(5) @(cb);

        $finish;
    end

    ////////////////////////////////////////////////////////////////////
    // Flop ready signal (ready latency is 1)
    ////////////////////////////////////////////////////////////////////
    always_ff @(cb) begin
        if(rst) begin
            in_ready_d1 <= 1'b0;
        end else begin
            in_ready_d1 <= s_tready;
        end
    end

    ////////////////////////////////////////////////////////////////////
    // Manage TB/DUT handshake
    ////////////////////////////////////////////////////////////////////
    always_comb begin
        s_tvalid = in_ready_d1 ? (($urandom_range(100) <= TVALID_PROB) && seq_started && !seq_ended) : 1'b0;
        inject = s_tvalid && in_ready_d1;
    end

    ////////////////////////////////////////////////////////////////////
    // Monitor output stream
    ////////////////////////////////////////////////////////////////////
    initial forever @(negedge clk iff msg_valid) begin
        $display("[%0t ns] msg_data=%h msg_length=%d msg_error=%b msg_valid=%b",
                  $time - CLK_PERIOD_BY_2, msg_data, msg_length, msg_error, msg_valid);
    end

    ////////////////////////////////////////////////////////////////////
    // Instantiate the DUT
    ////////////////////////////////////////////////////////////////////
    msg_parser #(
        .MIN_MSG_BYTES     (`MIN_MSG_BYTES),
        .MAX_MSG_BYTES     (`MAX_MSG_BYTES),
        .WIDTH_TDATA_BYTES (`WIDTH_TDATA_BYTES)
    ) dut(.*);

endmodule
