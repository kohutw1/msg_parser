#!/usr/bin/env bash

vivado \
    -mode batch \
    -source build.tcl \
    -nojournal \
    -nolog
