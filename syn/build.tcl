set top msg_parser

read_verilog \
    -sv \
    [ glob ../rtl/*.sv ]

read_xdc \
    ${top}.xdc

synth_design \
    -top $top \
    -part xcvu7p-flva2104-3-e \
    -directive PerformanceOptimized \
    -fsm_extraction one_hot \
    -resource_sharing off \
    -no_lc \
    -shreg_min_size 5 \
    -verilog_define MIN_MSG_BYTES=8 \
    -verilog_define MAX_MSG_BYTES=32 \
    -verilog_define WIDTH_TDATA_BYTES=8

opt_design \
    -directive ExploreWithRemap

place_design \
    -directive Explore

phys_opt_design \
    -directive AggressiveExplore

route_design \
    -directive AggressiveExplore

report_timing > timing.txt
